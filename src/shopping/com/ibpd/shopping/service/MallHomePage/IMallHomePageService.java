package com.ibpd.shopping.service.MallHomePage;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.MallHomePageEntity;

public interface IMallHomePageService extends IBaseService<MallHomePageEntity> {
	List<MallHomePageEntity> getListByType(Integer type,Integer rowCount);
}
