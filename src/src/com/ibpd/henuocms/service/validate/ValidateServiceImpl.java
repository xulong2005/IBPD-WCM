package com.ibpd.henuocms.service.validate;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.ValidateEntity;
@Service("validateService")
public class ValidateServiceImpl extends BaseServiceImpl<ValidateEntity> implements IValidateService {
	public ValidateServiceImpl(){
		super();
		this.tableName="ValidateEntity";
		this.currentClass=ValidateEntity.class;
		this.initOK();
	}
	
}
 