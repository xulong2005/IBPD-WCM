package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 系统属性配置
 * @author mg by qq:349070443
 *编辑于 2015-7-6 下午04:10:00
 */
@Entity
@Table(name="T_SysProperties")
public class SystemProperEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String from_email_account="from_email_account";
	public static final String from_email_password="from_email_password";
	public static final String from_eamil_smtpServer="from_eamil_smtpServer";
	public static final String system_fullname="system_fullname";
	public static final String system_smallname="system_smallname";
	public static final String enable_scale_image="enable_scale_image";
	public static final String scale_image_position_leftTop="scale_image_position_leftTop";
	public static final String scale_image_position_rightTop="scale_image_position_rightTop";
	public static final String scale_image_position_leftBottom="scale_image_position_leftBottom";
	public static final String scale_image_position_rightBottom="scale_image_position_rightBottom";
	public static final String scale_image_position_center="scale_image_position_center";
	public static final String header_text="header_text";
	public static final String footer_text="footer_text";
	public static final String scale_image_path="scale_image_path";
	public static final String uploadFile_fileName_rename="uploadFile_fileName_rename";
	public static final String enable_zip_image="enable_zip_image";
	/**
	 * cocntroller中根据这个来枚举系统配置选项，每个项根据tab分割，分别代表：key：displayName：dataType：默认值
	 */
	public static final String[] keys=new String[]{
		"system_fullname	系统全名称	string	IBPD-WCM系统",
		"system_smallname	系统短名称	string	IBPD-WCM系统",
		"header_text	管理页header标题	string	欢迎使用IBPDWCM系统",
		"footer_text	管理页底部标题	string	IBPD版权所有",
		"from_email_account	邮箱地址(用户名)	string	xxx@163.com",
		"from_email_password	邮箱密码	string	xxxxxxxx",
		"from_eamil_smtpServer	邮件发送服务器	string	smtp.163.com",
		"enable_scale_image	是否启用图片水印	boolean	启用:y;禁用:n",
		"scale_image_path	水印图片地址	string	/images/scale.png",
		"scale_image_position_leftTop	水印位置-左上	boolean	启用:y;禁用:n",
		"scale_image_position_rightTop	水印位置-右上	boolean	启用:y;禁用:n",
		"scale_image_position_leftBottom	水印位置-左下	boolean	启用:y;禁用:n",
		"scale_image_position_rightBottom	水印位置-右下	boolean	启用:y;禁用:n",
		"scale_image_position_center	水印位置-居中	boolean	启用:y;禁用:n",
		"uploadFile_fileName_rename	启用上传文件重命名	boolean	启用:y;禁用:n",
		"enable_zip_image	使能上传图片压缩(分辨率)	boolean	启用:y;禁用:n"
};
	public SystemProperEntity(){
		super();
	}
	public SystemProperEntity(String key,String disName,String value,String des,String type){
		this.keyName=key;
		this.displayName=disName;
		this.defaultValue=value;
		this.description=des;
		this.dataType=type;
	}
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_displayName",length=500,nullable=true)
	private String displayName=" ";
	@Column(name="f_dataType",length=50,nullable=true)
	private String dataType="";
	@Column(name="f_defaultValue",length=100,nullable=true)
	private String defaultValue="";
	public Long getId() {  
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getKeyName() {
		return keyName;
	}
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	@Column(name="f_keyName",length=200,nullable=true)
	private String keyName=" ";
	@Column(name="f_value",length=500,nullable=true)
	private String value=" ";
	@Column(name="f_description",length=500,nullable=true)
	private String description=" ";
}
