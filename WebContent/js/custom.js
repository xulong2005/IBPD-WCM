	function getSelections(gridId){
		var _sets=$("#"+gridId).datagrid("getSelections");
		var _ids="";
		$.each(_sets,function(i,n){
		_ids+=n.id+",";
		});
		return _ids;
	};
	var cmenu;
	function getcmenu(){
		return cment;
	}
    function createColumnMenu(tableGridId){
            cmenu = $('<div style="height:300px;overflow-y:scroll"/>').appendTo('body');
            cmenu.menu({
                onClick: function(item){
                    if (item.iconCls == 'icon-ok'){
                        $('#'+tableGridId).datagrid('hideColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-empty'
                        });
                    } else {
                        $('#'+tableGridId).datagrid('showColumn', item.name);
                        cmenu.menu('setIcon', {
                            target: item.target,
                            iconCls: 'icon-ok'
                        });
                    }
                }
       		});
            var fields = $('#'+tableGridId).datagrid('getColumnFields');
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $('#'+tableGridId).datagrid('getColumnOption', field);
                if(col.hidden){
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-empty'
	                });
                }else{
	                cmenu.menu('appendItem', {
	                    text: col.title,
	                    name: field,
	                    iconCls: 'icon-ok'
	                });
                }
            }
       };
	   	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
	function closeCurWindow(){ 
		window.opener='';
		window.open('','_self');
		window.close();
	};


