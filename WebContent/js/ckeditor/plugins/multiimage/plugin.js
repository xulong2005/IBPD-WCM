﻿CKEDITOR.plugins.add('multiimage',    
    {           
        requires : ['dialog'],    
        init : function (editor)    
        {    
            var pluginName = 'multiimage';    
                
            //加载自定义窗口，就是dialogs前面的那个/让我纠结了很长时间    
            CKEDITOR.dialog.add('myDialog',this.path + "/dialogs/mydialog.js");    
                
            //给自定义插件注册一个调用命令    
            editor.addCommand( pluginName, new CKEDITOR.dialogCommand( 'myDialog' ) );    
            dlgTitle:'aaa',
            //注册一个按钮，来调用自定义插件    
            editor.ui.addButton('MyButton',    
                    {    
                        //editor.lang.mine是我在zh-cn.js中定义的一个中文项，    
                        //这里可以直接写英文字符，不过要想显示中文就得修改zh-cn.js    
                        label : '多图片上传',    
                        command : pluginName,  
                        icon: this.path + 'images/icon.png'    
                    });    
        }    
    }    
);    